# KaMo Tugas Akhir PKWU

*KaMo adalah singkatan dari Kantin Moklet

*kamo.sql adalah database dari project ini menggunakan mysql

*folder kamo(php) adalah folder file PHP untuk digunakan APInya dengan bahasa PHP Native

*project ini dikerjakan dengan tim berisi 3 orang

*dokumentasi berupa tangkapan layar dapat dilihat di : https://drive.google.com/file/d/1ZtZvU-bPQQcirfjBccM55wDFTTT9Dnff/view?usp=sharing 

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
