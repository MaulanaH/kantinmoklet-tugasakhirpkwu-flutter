import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'manage.dart';

void main() => runApp(App());

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SmartWalletOnboardingPage(),
    );
  }
}

class SmartWalletOnboardingPage extends StatelessWidget {
  static final String path = "lib/src/pages/onboarding/smart_wallet_onboarding.dart";
  final pages = [
    PageViewModel(
      pageColor: Color(0xF6F6F7FF),
        bubbleBackgroundColor: Colors.orange[300],
        title: Container(),
        body: Column(
          children: <Widget>[
            Text('Welcome to Kamo'),
            Text(
              'Make you easier to pay',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 16.0
              ),
            ),
          ],
        ),
        mainImage: Image.asset(
          'assets/guide1.png',
          width: 285.0,
          alignment: Alignment.center,
        ),
        textStyle: TextStyle(color: Colors.black),
    ),
    PageViewModel(
      pageColor: Color(0xF6F6F7FF),
        iconColor: null,
        bubbleBackgroundColor: Colors.orange[300],
        title: Container(),
        body: Column(
          children: <Widget>[
            Text('Just Scan to Pay'),
            Text(
              'Pay some bills with just scan the QR code',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 16.0
              ),
            ),
          ],
        ),
        mainImage: Image.asset(
         'assets/guide2.png',
          width: 285.0,
          alignment: Alignment.center,
        ),
        textStyle: TextStyle(color: Colors.black),
    ),
    PageViewModel(
      pageColor: Color(0xF6F6F7FF),
        iconColor: null,
        bubbleBackgroundColor: Colors.orange[300],
        title: Container(),
        body: Column(
          children: <Widget>[
            Text('No More Wallet'),
            Text(
              'Now, you dont need bring wallet or money to pay',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 16.0
              ),
            ),
          ],
        ),
        mainImage: Image.asset(
          'assets/guide3.png',
          width: 285.0,
          alignment: Alignment.center,
        ),
        textStyle: TextStyle(color: Colors.black),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            IntroViewsFlutter(
              pages,
              onTapDoneButton: (){
                Navigator.of(context).push(
                  new MaterialPageRoute(
                    builder: (BuildContext context)=> new ManageView(),
                  )
                );
              },
              showSkipButton: false,
              doneText: Text("Get Started",),
              pageButtonsColor: Colors.orange[300],
              pageButtonTextStyles: new TextStyle(
                  fontSize: 16.0,
                  fontFamily: "Regular",
              ),
            ),
            Positioned(
              top: 20.0,
              left: MediaQuery.of(context).size.width/2 - 50,
              child: Image.asset('assets/logo.png', width: 100,)
            )
          ],
        ),
      ),
    );
  }
}