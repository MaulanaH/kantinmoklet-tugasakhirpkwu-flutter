import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:kamo/saldouser.dart';

class Saldo extends StatefulWidget {
  Saldo({Key key}) : super(key: key);

  @override
  _SaldoState createState() => _SaldoState();
}

class _SaldoState extends State<Saldo> {
  Future<List> getSaldo() async {
    final response = await http.get("http://10.0.2.2/kamo/getdata.php");
    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Saldo Kantin Ku"),
      ),
      body: new FutureBuilder<List>(
        future: getSaldo(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? new ItemList(
                  list: snapshot.data,
                )
              : new Center(
                  child: new CircularProgressIndicator(),
                );
        },
      ),
    );
  }
}

class ItemList extends StatelessWidget {
  final List list;
  ItemList({this.list});

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: list == null ? 0 : list.length,
      itemBuilder: (context, i) {
        return Column(
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.all(8.0),
              child: new Card(
                child: new ListTile(
                  title: new Text("Kantin Pak Yoyok"),
                  leading: new Icon(Icons.widgets),
                  subtitle: new Text("Saldo : ${list[i]['saldo']}"),
                ),
              ),
            ),
            new RaisedButton(
                child: new Text("Saldo User Ku?"),
                color: Colors.blue,
                onPressed: () =>
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => new SaldoUser(),
                    )))
          ],
        );
      },
    );
  }
}
