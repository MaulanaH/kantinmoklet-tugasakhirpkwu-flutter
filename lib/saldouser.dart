import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class SaldoUser extends StatefulWidget {
  SaldoUser({Key key}) : super(key: key);

  @override
  _SaldoUserState createState() => _SaldoUserState();
}

class _SaldoUserState extends State<SaldoUser> {
  Future<List> getSaldoUser() async {
    final response = await http.get("http://10.0.2.2/kamo/getdatauser.php");
    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Saldo User Ku"),
      ),
      body: new FutureBuilder<List>(
        future: getSaldoUser(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? new ItemList(
                  list: snapshot.data,
                )
              : new Center(
                  child: new CircularProgressIndicator(),
                );
        },
      ),
    );
  }
}

class ItemList extends StatelessWidget {
  final List list;
  ItemList({this.list});

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: list == null ? 0 : list.length,
      itemBuilder: (context, i) {
        return new Container(
          padding: const EdgeInsets.all(8.0),
          child: new Card(
            child: new ListTile(
              title: new Text("Ini Sisa Uangku"),
              leading: new Icon(Icons.widgets),
              subtitle: new Text("Saldo : ${list[i]['saldo']}"),
            ),
          ),
        );
      },
    );
  }
}
